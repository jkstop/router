package com.github.jkstop.router.details

interface DetailRouter {
    fun setRootFragment(id: String)
}