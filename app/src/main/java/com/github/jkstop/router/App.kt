package com.github.jkstop.router

import android.app.Application
import android.content.Context
import toothpick.Toothpick
import toothpick.config.Module

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        Toothpick.openScope("app")
            .apply {
                installModules(
                    AppModule(this@App)
                )
            }
    }

    inner class AppModule(
        context: Context
    ) : Module() {

        init {
            bind(Context::class.java)
                .toInstance(context)
        }
    }


}