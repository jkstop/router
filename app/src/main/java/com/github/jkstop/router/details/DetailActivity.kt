package com.github.jkstop.router.details

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.github.jkstop.router.FragmentRunnerParams
import com.github.jkstop.router.R
import toothpick.Toothpick
import toothpick.config.Module
import javax.inject.Inject

class DetailActivity : AppCompatActivity() {

    @Inject
    lateinit var router: DetailRouter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        // TODO move to base
        Toothpick.openScopes("app", this).apply {
            installModules(object : Module() {
                init {

                    bind(FragmentRunnerParams::class.java)
                        .toInstance(
                            FragmentRunnerParams(
                                fm = supportFragmentManager,
                                container = R.id.content
                            )
                        )

                    bind(DetailRouter::class.java)
                        .to(DetailRouterImpl::class.java)
                }
            })
        }.apply {
            Toothpick.inject(this@DetailActivity, this)
        }

        router.setRootFragment(
            intent.getStringExtra("ID")
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        Toothpick.closeScope(this)
    }

    companion object {
        fun newIntent(context: Context, id: String): Intent {
            return Intent(context, DetailActivity::class.java)
                .putExtra("ID", id)
        }
    }
}