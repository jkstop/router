package com.github.jkstop.router.main

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.github.jkstop.router.R
import kotlinx.android.synthetic.main.activity_main.*
import toothpick.Toothpick
import toothpick.config.Module
import java.util.UUID
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var router: MainRouter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // TODO move to base
        Toothpick.openScopes("app", this).apply {
            installModules(object : Module() {
                init {
                    bind(Activity::class.java)
                        .toInstance(this@MainActivity)

                    bind(MainRouter::class.java)
                        .to(MainRouterImpl::class.java)
                }
            })
        }.apply {
            Toothpick.inject(this@MainActivity, this)
        }

        vGo.setOnClickListener {
            router.openDetails(UUID.randomUUID().toString())
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        Toothpick.closeScope(this)
    }
}
