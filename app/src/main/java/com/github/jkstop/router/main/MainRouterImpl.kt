package com.github.jkstop.router.main

import android.app.Activity
import com.github.jkstop.router.ActivityRunner
import com.github.jkstop.router.details.DetailActivity
import javax.inject.Inject

class MainRouterImpl @Inject constructor(
    private val activity: Activity
) : MainRouter, ActivityRunner(activity) {

    override fun openDetails(id: String) = startActivity(
        DetailActivity.newIntent(activity, id)
    )

}