package com.github.jkstop.router

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager.POP_BACK_STACK_INCLUSIVE

open class FragmentRunner(
    private val params: FragmentRunnerParams
) {

    fun replaceFragment(fragment: Fragment) {
        params.fm
            .beginTransaction()
            .replace(params.container, fragment, fragment.tag)
            .commitNow()
    }

    fun replaceFragmentWithStack(fragment: Fragment, tag: String) {
        params.fm
            .beginTransaction()
            .replace(params.container, fragment, fragment.tag)
            .addToBackStack(tag)
            .commitNow()
    }

    fun addFragment(fragment: Fragment) {
        params.fm
            .beginTransaction()
            .add(params.container, fragment, fragment.tag)
            .commitNow()
    }

    fun removeFragment(fragment: Fragment) {
        params.fm
            .beginTransaction()
            .remove(fragment)
            .commitNow()
    }

    fun popStackToFragment(fragment: Fragment) {
        params.fm
            .popBackStack(fragment.tag, POP_BACK_STACK_INCLUSIVE)
    }
}