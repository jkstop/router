package com.github.jkstop.router

import androidx.fragment.app.FragmentManager

data class FragmentRunnerParams(
    val fm: FragmentManager,
    val container: Int
)