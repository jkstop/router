package com.github.jkstop.router

import android.app.Activity
import android.content.Intent

open class ActivityRunner(
    private val parentContext: Activity
) {

    fun startActivity(intent: Intent) {
        parentContext.startActivity(intent)
    }

    fun startActivityForResult(intent: Intent, requestCode: Int) {
        parentContext.startActivityForResult(intent, requestCode)
    }
}