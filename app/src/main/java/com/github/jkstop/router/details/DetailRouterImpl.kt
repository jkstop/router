package com.github.jkstop.router.details

import com.github.jkstop.router.FragmentRunner
import com.github.jkstop.router.FragmentRunnerParams
import javax.inject.Inject

class DetailRouterImpl @Inject constructor(
    params: FragmentRunnerParams
) : DetailRouter, FragmentRunner(params) {

    override fun setRootFragment(id: String) {
        replaceFragment(DetailFragment.newInstance(id))
    }
}