package com.github.jkstop.router.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.github.jkstop.router.R

class DetailFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val id = arguments?.getString("ID").orEmpty()

        println("qqqqq id $id")
    }

    companion object {
        fun newInstance(id: String) = DetailFragment().apply {
            arguments = Bundle().apply {
                putString("ID", id)
            }
        }
    }
}