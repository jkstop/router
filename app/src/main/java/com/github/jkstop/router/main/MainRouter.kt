package com.github.jkstop.router.main

interface MainRouter {

    fun openDetails(id: String)
}